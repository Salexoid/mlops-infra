# MLOps Infra

## MLflow

**MLflow Scenario: 4**

Mlflow address (accessible): http://195.122.250.218:5000

![Mlflow](samples/mlflow.png)

## Minio S3

![Minio](samples/minio.png)

## Postges Admin Panel

![PGAdmin](samples/pgadmin.png)

## Grafana Monitoring with Prometheus

![Grafana with Prometheus](samples/grafana.png)

## Portainer Overview

![Portainer - all containers](samples/portainer.png)
